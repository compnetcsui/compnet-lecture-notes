**Module 3 - Transport Layer**

Next thing is transport layer. We could use an analogy with a household analogy.

There are these 12 kids in Ann’s house sending letters to 12 kids in Bill’s house. As we scrutinize it;



* Hosts = houses
* Processes = kids
* App messages = letters in envelopes.

Network layer → is the logical communication term between those hosts.

Transport layer → Logical communication term between those processes.

**KEY EXPLANATION:**



* Takes data from higher levels of OSI Model and breaks it into segments that can
* be sent to lower-level layers for data transmission
*  Conversely, reassembles data segments into data that higher-level protocols and applications can use
*  Also puts segments in correct order (called sequencing ) so they can be reassembled in correct order at destination
*  Concerned with the reliability of the transport of sent data
*  May use a connection-oriented protocol such as TCP to ensure destination received segments
*  May use a connectionless protocol such as UDP to send segments without assurance of delivery
*  Uses port addressing
* The transport layer provides several services to applications, including data segmentation, multiplexing, and error recovery.
* The transport layer is implemented in software, and operates above the network layer and below the application layer in the protocol stack.

There are these 2 things that is important in Transport Layer Topic:

1. Transport layer protocols:



* Transmission Control Protocol (TCP): TCP is a connection-oriented transport protocol that provides reliable, ordered delivery of data between applications. TCP uses a three-way handshake to establish a connection, and provides flow control, congestion control, and error recovery mechanisms.
* User Datagram Protocol (UDP): UDP is a connectionless transport protocol that provides unreliable, unordered delivery of data between applications. UDP does not provide any error recovery mechanisms, and is often used for applications that require low-latency communication or do not require reliable data delivery.

2. Multiplexing and Demultiplexing



* Whenever an entity accepts items from more than one source, this is referred to as **multiplexing (many to one).**
*  Whenever an entity delivers items to more than one source, this is referred to as **demultiplexing (one to many).**
*  The transport layer at the source performs multiplexing
*  The transport layer at the destination performs demultiplexing



![alt_text](./img/image13.png "image_tooltip")


Let’s talk more about Multiplexing and Demultiplexing before we go onto the next thing:

**Multiplexing:**



* Multiplexing is the process of combining data from multiple applications into a single stream of data for transmission over a network.
* In the transport layer, multiplexing is achieved by using different source and destination port numbers for each application.
* Each application is assigned a unique source port number by the operating system when it opens a network connection, and this port number is used to identify the application on the local host.
* When the data from multiple applications is combined into a single stream of data, each segment is marked with the appropriate source and destination port numbers.

**Demultiplexing:**



* Demultiplexing is the process of separating data from a single stream of data received over a network into individual streams of data for delivery to the appropriate application.
* In the transport layer, demultiplexing is achieved by using the destination port number to identify the application to which the data should be delivered.
* When a segment is received over the network, the transport layer examines the destination port number to determine the application to which the segment should be delivered.
* The segment is then forwarded to the appropriate application on the local host using the corresponding source port number.

**Let me give you an example:**

**Suppose** a host has two applications, a web browser and an email client, both of which are using the TCP transport protocol. The web browser might be assigned the source port number 1234 by the operating system, and might be communicating with a web server on the destination port number 80.

The email client might be assigned the source port number 5678 by the operating system, and might be communicating with a mail server on the destination port number 25. When the host sends data over the network, each segment is marked with the appropriate source and destination port numbers. When the host receives data over the network, the transport layer examines the destination port number to determine whether the data should be delivered to the web browser or the email client.

Next, let’s talk about TCP and UDP;

**—-------------------TCP----------------------------**

**TCP Services:**

Process-to-Process Communication  



* TCP provides process-to-process communication using port numbers. 

Stream Delivery Service  



* TCP is a stream-oriented protocol.  
* TCP allows the sending process to deliver data as a stream of bytes and allows the receiving process to obtain data as a stream of bytes.  
* TCP creates an environment in which the two processes seem to be connected by an imaginary “tube” that carries their bytes across the Internet.  
* The sending process produces (writes to) the stream and the receiving process consumes (reads from) it.

Full-Duplex Communication  



* TCP offers full-duplex service, where data can flow in both directions at the same time.  
* Each TCP endpoint then has its own sending and receiving buffer, and segments move in both directions

Multiplexing and Demultiplexing 



* TCP performs multiplexing at the sender and demultiplexing at the receiver. 

Connection-Oriented Service  



* TCP is a connection-oriented protocol.  
* A connection needs to be established for each pair of processes.  
* When a process at site A wants to send to and receive data from another process at site B, the following three phases occur: 

    1. The two TCP’s establish a logical connection between them. 2. Data are exchanged in both directions. 


    3. The connection is terminated.


Reliable Service  



* TCP is a reliable transport protocol.  
* It uses an acknowledgment mechanism to check the safe and sound arrival of data

In TCP, there is this term that is called **TCP Segments**:

Segment is a packet in TCP.

TCP Packet Format:



![alt_text](./img/image14.png "image_tooltip")


**SrcPort and DstPort―**port number of source and destination process. **SequenceNum―**contains sequence number, i.e. first byte of data segment. **Acknowledgment― **byte number of segment, the receiver expects next. **HdrLen―**Length of TCP header as 4-byte words. 

**Flags―** contains six control bits known as flags. 

o URG — segment contains urgent data. 

o ACK — value of acknowledgment field is valid. 

o PUSH — sender has invoked the push operation. 

o RESET — receiver wants to abort the connection. 

o SYN — synchronize sequence numbers during connection establishment. 

o FIN — terminates the TCP connection.

**Advertised Window―**defines receiver’s window size and acts as flow control. 

**Checksum―**It is computed over TCP header, Data, and pseudo header containing IP fields (Length, SourceAddr & DestinationAddr). 

**UrgPtr ― **used when the segment contains urgent data. It defines a value that must be added to the sequence number. Options - There can be up to 40 bytes of optional information in the TCP header. 

**TCP Connection Management:**



![alt_text](./img/image15.png "image_tooltip")


**TCP (Transmission Control Protocol) uses a three-way handshake** to establish a connection between two devices. The three-way handshake is a process of exchanging messages between the sender and receiver to establish a reliable connection.

The three steps of the TCP three-way handshake are:

SYN (Synchronize): The initiating device sends a SYN segment to the receiver to initiate the connection. The SYN segment includes a sequence number that the receiver will use to synchronize the connection.

SYN-ACK (Synchronize-Acknowledge): The receiver responds with a SYN-ACK segment, which acknowledges the SYN segment and includes a sequence number that the initiating device will use to synchronize the connection.

ACK (Acknowledge): The initiating device sends an ACK segment to acknowledge the SYN-ACK segment and complete the connection establishment.

Once the connection is established, data can be transmitted between the devices. TCP uses sequence numbers and acknowledgment numbers to ensure that the data is delivered reliably and in the correct order.

To terminate a TCP connection, a four-way handshake is used. The four steps of the TCP four-way handshake are:

FIN (Finish): The initiating device sends a FIN segment to the receiver to initiate the connection termination.

ACK: The receiver sends an ACK segment to acknowledge the FIN segment.

FIN: The receiver sends a FIN segment to initiate the termination of its end of the connection.

ACK: The initiating device sends an ACK segment to acknowledge the FIN segment and complete the connection termination.

During the connection termination process, data can still be transmitted until both devices have sent a FIN segment and received an ACK segment.

So basically, TCP connection management is the process of establishing and terminating a reliable connection between two devices. TCP uses a three-way handshake to establish a connection and a four-way handshake to terminate a connection. Once the connection is established, data is transmitted using sequence numbers and acknowledgment numbers to ensure reliable delivery.

**TCP Flow Control:**



* Flow Control is the process of managing the rate of data transmission between two nodes to prevent a fast sender from overwhelming a slow receiver.  
* It provides a mechanism for the receiver to control the transmission speed, so that the receiving node is not overwhelmed with data from transmitting node.



![alt_text](./img/image16.png "image_tooltip")


**KEY EXPLANATION ABOUT FLOW CONTROL:**

Without flow control, the receiver's buffer can overflow, and frames can get lost. To overcome this problem, the data link layer uses the flow control to prevent the sending node on one side of the link from overwhelming the receiving node on another side of the link. This prevents traffic jam at the receiver side.

**—-------------------UDP----------------------------**



![alt_text](./img/image17.png "image_tooltip")


**UDP PORTS:**



* Processes (server/client) are identified by an abstract locator known as port.  
* Server accepts message at well known port.  
* Some well-known UDP ports are 7–Echo, 53–DNS, 111–RPC, 161–SNMP, etc.  
* &lt; port, host > pair is used as key for demultiplexing.  
* Ports are implemented as a message queue.  
* When a message arrives, UDP appends it to end of the queue.  
* When queue is full, the message is discarded.  
* When a message is read, it is removed from the queue.  
* When an application process wants to receive a message, one is removed from the front of the queue.  
* If the queue is empty, the process blocks until a message becomes available.

**UDP FORMAT:**



![alt_text](./img/image18.png "image_tooltip")


Src Port Number:



* Port number used by process on source host with 16 bits long. 
*  If the source host is client (sending request) then the port number is an temporary one requested by the process and chosen by UDP.  
* If the source is server (sending response) then it is well known port number. 

Dst Port Number:



* Port number used by process on Destination host with 16 bits long.  
* If the destination host is the server (a client sending request) then the port number is a well known port number.  
* If the destination host is client (a server sending response) then port number is an temporary one copied by server from the request packet.

**CHECKSUM**

The UDP checksum calculation is different from the one for IP and ICMP. Here the checksum includes three sections: a pseudo header, the UDP header, and the data coming from the application layer. The pseudo header is the part of the header of the IP packet in which the user datagram is to be encapsulated with some fields filled with Os 

If the checksum does not include the pseudo header, a user datagram may arrive safe and sound. However, if the IP header is corrupted, it may be delivered to the wrong host. The protocol field is added to ensure that the packet belongs to UDP, and not to other transport-layer protocols. 



![alt_text](./img/image19.png "image_tooltip")


**UDP SERVICES:**

**Process-to-Process Communication **

UDP provides process-to-process communication using socket addresses, a combination of IP addresses and port numbers. 

**Connectionless Services **

**UDP **provides a connectionless service. This means that each user datagram sent by UDP is an independent datagram. There is no relationship between the different user datagrams even if they are coming from the same source process and going to the same destination program. The user datagrams are not numbered. Also, unlike TCP, there is no connection establishment and no connection termination. This means that each user datagram can travel on a different path. 

**Flow Control **

UDP is a very simple protocol. There is no flow control, and hence no window mechanism. The receiver may overflow with incoming messages. The lack of flow control means that the process using UDP should provide for this service, if needed. 

**Error Control **

There is no error control mechanism in UDP except for the checksum. This means that the sender does not know if a message has been lost or duplicated. When the receiver detects an error through the checksum, the user datagram is silently discarded. The lack of error control means that the process using UDP should provide for this service, if needed. 

**Congestion Control **

Since UDP is a connectionless protocol, it does not provide congestion control. UDP assumes that the packets sent are small and sporadic and cannot create congestion in the network. 

**Encapsulation and Decapsulation **

To send a message from one process to another, the UDP protocol encapsulates and decapsulates messages.