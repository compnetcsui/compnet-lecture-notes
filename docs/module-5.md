**Module 5 - Internet & Routing Protocol**

Before we step into NAT and IPv6, let’s discuss something about IP again.

IP (Internet Protocol) is a fundamental protocol for communication purposes within computer networks.


**NAT**

NAT is a technique used in IP networking to enable multiple devices in a private network to share a single public IP address. It addresses the scarcity of public IPv4 addresses by translating private IP addresses to a public IP address when communicating with the internet. NAT operates at the network layer of the TCP/IP protocol stack.

Class A addresses: 10.0.0.0 – 10.255.255.255.255
Class B addresses: 172.16.0.0 – 172.31. 255.255
Class C addresses: 192.168.0.0 – 192.168.255.255

As the internal network addresses are reserved and not routable, the Firewall undertakes the task of translating internal addresses to external IP addresses and vice versa. This translation enables communication between the private network and the public network. And also, NAT allows the use of the same IP addresses in different private networks. This prolongs the life expectancy of IPv4 on the internet. Without NAT the supply of IP addresses would have exhausted long back.

Here's an example of a NAT function.

![alt_text](./img/image23.jpg "image_tooltip")

**IPv6**

IPv6 is the most recent version of the Internet Protocol (IP) and is intended to replace IPv4, which is based on 32-bit addressing and is limited to a total of 4.3 billion addresses. In contrast, IPv6 is based on 128-bit addressing and can support 340 undecillion, which is 340 trillion3 addresses.

IPv6 addresses are represented as eight groups of four hexadecimal digits each, separated by colons. The full representation may be shortened. For example, 2001:0db8:0000:0000:0000:8a2e:0370:7334 becomes 2001:db8::8a2e:370:7334.

- Addressing

IPv6 addresses are 128-bits long and are identifiers for individual interfaces and sets of interfaces.  Since each interface belongs to a single node, any of that node's interfaces' unicast addresses can be used as an identifier for the node.  A single interface may be assigned multiple IPv6 addresses of any type. There are 3 types of addresses: unicast, anycast and multicast.  Approximately 15% of the address space is initially allocated to the reserved addresses such as NSAP  (Network Access Service Point) addresses,IPX (Internetworking Packet Exchange) addresses, etc.  The remaining 85% is reserved for future use. 

Unicast:   An identifier for a single interface.  A packet sent to a unicast address is delivered to the interface identified by that address.

Anycast:   An identifier for a set of interfaces (typically belonging to different nodes).  A packet sent to an anycast address is delivered to one   of the interfaces identified by that address (the "nearest" one, according to the routing protocols' measure of distance).

Multicast: An identifier for a set of interfaces (typically belonging to different nodes).  A packet sent to a multicast address is delivered to all interfaces identified by that address.

- Header format

![alt_text](./img/image24.jpg "image_tooltip")

VERS
identifies protocol as version 6

PRIORITY
Identifies Priority characteristics.

PAYLOAD LENGTH
Specifies only the size of the data being carried, it does not include the header.

HOP LIMIT
Corresponding to IP's TIME TO LIVE, datagram is discarded if HOP LIMIT counts down to zero.

FLOW LABEL
Divided into two parts, one used to define a specific path, the other to specify a traffic class. Routers use the value in the FLOW LABEL field to route the datagram.

NEXT HEADER
Used to specify the type of information that follows the current header.

**Routing Protocols**

In Routing Protocol, Routing is the process of selecting paths in a network along which to send data on physical traffic. In different network operating system the network layer perform the function of protocol routing. In TCP/IP the IP protocol is the ability to form connections between different physical networks with the help of a Routing Protocol.

When it comes to routing protocols, there are several popular options to choose from, but the three most commonly used in enterprise networks are EIGRP, OSPF, and BGP. Each protocol has its strengths and weaknesses, and the best choice will depend on the specific needs of the network.

There are two types of routing algorithm:
- Static
  Static routing uses pre-programmed definitions representing paths through the network. Static routing is manually performed by the network administrator. The administrator is responsible for discovering and propagating routes through the network.
- Dynamic
  Dynamic routing algorithms allow routers to automatically discover and maintain awareness of the paths through the network. This automatic discovery can use a number of currently available dynamic routing protocols.

The most commonly used routing protocols are as follows:
- RIP
  A distance vector interior routing protocol
- IGRP
  The   distance   vector   interior   routing   protocol   developed   by   Cisco
- OSPF
  A link-state interior routing protocol
- IS-IS
  A link-state interior routing protocol
- EIGRP
  The advanced distance vector interior routing protocol developed by Cisco
- BGP
  A path vector exterior routing protocol

![alt_text](./img/image25.jpg "image_tooltip")

In distance vector routing, the least-cost route between any two nodes is the route with
minimum distance. In this protocol, as the name implies, each node maintains a vector (table)
of minimum distances to every node.

There are mainly 3 things on this routing algorithm:
1. Initialization
2. Sharing
3. Updating

**Initialization**

In this scenario, each node has limited knowledge of the distances between itself and its immediate neighbors, which are the nodes directly connected to it. Therefore, we assume that each node is capable of sending a message to its immediate neighbors to determine the distance between itself and those neighbors. 

The initial tables for each node are depicted in the figure below. For entries that do not correspond to neighbor nodes, the distance is indicated as infinite, signifying that they are unreachable.

![alt_text](./img/image26.jpg "image_tooltip")

**Sharing**

The fundamental concept of distance vector routing involves the exchange of information among neighboring nodes. For instance, while node A may not be aware of the existence of node E, node C possesses that knowledge. By sharing its routing table with node A, node C enables node A to also learn how to reach node E. Conversely, node C may not know the path to node D, but node A does. Through the sharing of its routing table with node C, node A enables node C to determine how to reach node D. In essence, by aiding each other, nodes A and C, as immediate neighbors, can enhance their routing tables. It is important to note that in distance vector routing, nodes periodically exchange their routing tables with their immediate neighbors and do so promptly in the event of any changes.

**Updating**

When a node receives a two-column table from a neighbor, it needs to update its routing
table. Updating takes three steps:

1. The receiving node needs to add the cost between itself and the sending node to each value in the second column. (x+y)
   
2. If the receiving node uses information from any row. The sending node is the next node in the route.
   
3. The receiving node needs to compare each row of its old table with the corresponding row of the modified version of the received table.
    a. If the next-node entry is different, the receiving node chooses the row with the smaller cost. If there is a tie, the old one is kept.
    b. If the next-node entry is the same, the receiving node chooses the new row.

For example, suppose node C has previously advertised a route to node X with distance 3.
Suppose that now there is no path between C and X; node C now advertises this route with a
distance of infinity. Node A must not ignore this value even though its old entry is smaller. The old route does not exist anymore. The new route has a distance of infinity.
Here is the status on updating:

![alt_text](./img/image27.jpg "image_tooltip")

After doing updating, here is the final diagram:

![alt_text](./img/image28.jpg "image_tooltip")

**Open Shortest Path First (OSPF)**

Open Shortest Path First (OSPF) is a link-state routing protocol that was developed for IP networks and is based on the Shortest Path First (SPF) algorithm. OSPF is an Interior Gateway Protocol (IGP), and it is generally considered easier to deploy and manage compared to BGP. OSPF maintains a database of locally derived and learned networks, and if multiple OSPF paths to remote networks exist, OSPF chooses a path based on the lowest calculated cost or metric. The path cost to a remote network is based on the total calculated reference bandwidth divided by interface bandwidth. Reference bandwidth is a number used within OSPF to quantify the speed of a link. 

Hierarchical OSPF (Open Shortest Path First) is a routing protocol that organizes OSPF networks into a hierarchical or two-level hierarchy structure. This hierarchical design helps to improve scalability and reduce the complexity of OSPF routing tables.

In a two-level hierarchy, OSPF networks are divided into two main levels: the backbone area (Level 0) and non-backbone areas (Level 1). The backbone area serves as the core routing infrastructure and connects multiple Level 1 areas.

The key features and components of the hierarchical OSPF with a two-level hierarchy include:

![alt_text](./img/image29.jpg "image_tooltip")

Backbone Area (Level 0):

The backbone area is the highest level in the hierarchy.
It connects various Level 1 areas and acts as the central routing domain.
Its primary function is to exchange routing information between Level 1 areas.
The backbone area is identified by the Area ID 0.0.0.0.

Non-Backbone Areas (Level 1):

Level 1 areas are linked to the backbone area.
Each Level 1 area has a unique Area ID within the OSPF domain.
Non-backbone areas handle local routing within their respective areas.
They exchange summarized routing information with the backbone area.

Area Border Routers (ABRs):

ABRs connect multiple areas and act as gateways between Level 1 areas and the backbone area.
They maintain separate databases for each connected area and perform route summarization between areas.

Autonomous System Boundary Routers (ASBRs):

ASBRs connect OSPF domains with external networks or non-OSPF networks.
They exchange routing information between OSPF and external networks using external routing protocols.

**BGP**

Unlike OSPF, BGP is an exterior gateway protocol optimized for routing between large networks. BGP excels with dynamic routing for large networks, while OSPF offers more efficient path choice and convergence speed.

How does BGP work?
When you have a network router that connects to other networks, it does not know which network is the best one to send its data to. BGP takes into consideration all the different peering options a router has and chooses the one closest to where the router is. Each potential peer communicates the routing information it has and that gets stored within a routing information base (RIB). BGP can access this information and use it to choose the best peering option.

A Simple Route
Destination pre-fix: (e.g., 128.112.0.0/16) 
Route attributes, including
AS path (e.g., “7018 88”)
Next-hop IP address (e.g., 12.127.0.121)

![alt_text](./img/image30.jpg "image_tooltip")

**BGP Attributes**
● Local pref: Statically configured ranking of routes
within AS
● AS path: ASs the announcement traversed
● Origin: Route came from IGP or EGP
● Multi Exit Discriminator: preference for where to exit
network
● Community: opaque data used for inter-ISP policy
● Next-hop: where the route was heard from

**Characteristics of BGP**
- Inter-Autonomous System Configuration
  The main role of BGP is to provide communication between two autonomous systems.
- BGP supports Next-Hop Paradigm.
  Coordination among multiple BGP speakers within the AS (Autonomous System).
- Path Information
  BGP advertisement also include path information, along with the reachable destination and next destination pair.
- Policy Support
  BGP can implement policies that can be configured by the administrator. For ex: a router running BGP can be configured to distinguish between the routes that are known within the AS and that which are known from outside the AS.
- Runs Over TCP.
- BGP conserve network Bandwidth.
- BGP supports CIDR.
- BGP also supports Security.

Here are some key points to understand BGP messages:
- BGP messages are divided into four types: OPEN, UPDATE, KEEPALIVE, and NOTIFICATION.
- The OPEN message is used to establish a BGP communication session between peers. This message contains important information such as the autonomous system number of the peer, hold time, and BGP identifier.
- The UPDATE message is used to exchange routing information, such as announcing new routes or withdrawing previously announced routes.
- The KEEPALIVE message is used to notify BGP peers that a device is active and to keep sessions from expiring.
- The NOTIFICATION message is used to close an active session and inform the peer of the reason why, in case of an error condition.
- All BGP messages have a common header that includes a marker field, a length field, and a type field. The marker field is deprecated but still exists for backward compatibility with older implementations. The length field indicates the total length of the message, including the header, in octets. The type field specifies the message type.
- The BGP header is followed by data, except in the case of a KEEPALIVE message, where there is no data required.
- The BGP message size can range from 19 to 4096 octets.

**Information Management Functions of BGP Route**

Route Storage
With route storage, individual BGPs keep information regarding how to connect with networks within a set of databases. Databases are also used to store routing information that can be accessed by BGP.

Route Update
BGP delivers update messages to advertise pertinent routing information. These are stored in a routing table that becomes available after the system has started up.

Route Selection
The BGP speaker, which advertises routes, only conveys information about the best route to peers.

Route Advertisement
When there is more than one feasible route, BGP only advertises the best one to peers. This helps the network function more efficiently because only viable routes are advertised.

