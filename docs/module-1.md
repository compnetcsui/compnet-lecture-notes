**Module 1 - Computer Networks & The Internet**

A computer network is a collection of devices connected together to exchange data and resources.

**Protocols:**

- Protocols are sets of rules and procedures that enable communication between devices in a network.

- They define how data is formatted, transmitted, received, and acknowledged by devices on the network.

- Protocols ensure that communication between devices is reliable, efficient, and secure.

- Common network protocols include TCP/IP, HTTP, FTP, SMTP, and POP.


![alt_text](./img/image1.png "image_tooltip")


Key explanation: Without a protocol, two devices may be connected but not communicating, just as a person speaking French cannot be understood by a person who speaks only Japanese. 

**TCP/IP:**

- TCP/IP stands for Transmission Control Protocol/Internet Protocol.

- It is the most widely used protocol for connecting devices to the internet.

- TCP is responsible for establishing connections, ensuring reliable data transmission, and error correction.

- IP is responsible for routing data between networks, and provides addressing and identification for devices on the network.

Network edge refers to the end systems and also hosts that are connected to the Internet. And it includes personal computers, servers, mobile devices, and other devices that generate or consume network traffic.

**Access Networks:**

- Access networks connect end systems (such as computers, mobile devices, and servers) to the internet.

- Access networks can be classified as wired or wireless.

- Wired access networks use physical cables (such as twisted-pair copper, coaxial cable, or fiber optic cable) to connect end systems to the internet.

- Wireless access networks use wireless signals (such as radio waves) to connect end systems to the internet.

- Access networks are usually owned and managed by internet service providers (ISPs), who provide internet connectivity to end users.

- Types of access networks:



* Ethernet – It is the most commonly installed wired LAN technology and it provides services on the Physical and Data Link Layer of OSI reference model. 
* DSL – DSL stands for Digital Subscriber Line and DSL brings a connection into your home through telephone lines and a DSL line can carry both data and voice signals and the data part of the line is continuously connected. DSL modem uses the telephone lines to exchange data with digital subscriber line access multiplexer (DSLAMs). In DSL we get 24 Mbps downstream and 2.5 Mbps upstream.
* FTTH – Fiber to the home (FTTH) uses optical fiber from a central Office (CO) directly to individual buildings and it provides high-speed Internet access among all access networks.It ensures high initial investment but lesser future investment and it is the most expensive and most future-proof option amongst all these access networks.
* Wireless LANs – It links two or more devices using wireless communication within a range. It uses high-frequency radio waves and often includes an access point for connecting to the Internet.
* 3G and LTE – It uses cellular telephony to send or receive packets through a nearby base station operated by the cellular network provider. Long Term Evolution (LTE) offers high-speed wireless communication for mobile devices and increased network capacity.

**Core Network:**

- Core network or network core is part of network that interconnects the internet's end systems which exchange messages with each other.

- The source or host breaks long messages into smaller part of data known as packets to end system destination. Packets are transmitted over each communication links and packet switches(routers and link-layer switches) across links on path from source to destination.

There are two key core network functions - Forwarding (switching): after the router has received all of the packet's bits, it begin to transmit (forward) the first bit of the packet onto the outbound link.

- Routing: determine source-destination paths taken by packets. A router takers a packet arriving on one of its attached communication links and forwards that packet onto another one of its attached communication links. In the internet, every end system has an address called an IP address. The source or host includes the destination’s IP address in the packet’s header when it wants to send a packet to a destination.



![alt_text](./img/image2.png "image_tooltip")


**Packet switching: store-and-forward**



![alt_text](./img/image3.png "image_tooltip")


A host with a packet to send transmits it to the nearest router, either on its own LAN or over a point-to-point link to the ISP. The packet is stored there until it has fully arrived and the link has finished its processing by verifying the checksum. Then it is forwarded to the next router along the path until it reaches the destination host, where it is delivered.

**Packet switching: packet queueing and loss**



![alt_text](./img/image4.png "image_tooltip")


If arrival rate (in bits) to link exceeds transmission rate of link for a period of time:  



* packets will queue, wait to be transmitted link 
* packets can be dropped (lost) if memory (buffer) fills up

**Circuit switching**

- A circuit-switched network consists of a set of switches connected by physical links.  

- A connection between two stations is a dedicated path made of one or more links  each connection uses only one dedicated channel on each link  

- Each link is normally divided into n channels by using FDM or TDM.  

- The link can be permanent (leased line) or temporary (telephone) 



![alt_text](./img/image5.png "image_tooltip")


There are two technologies used to implement this circuit switching, namely **Space Division and Time Division Switching.**

**Here are the comparison between Circuit Switching and Packet Switching:**



![alt_text](./img/image6.png "image_tooltip")


Illustration for Circuit Switching:



![alt_text](./img/image7.png "image_tooltip")


Circuit switching networks can be used after establishing the connection between the nodes. Circuit switching networks are ideal for real time data transmission, such as standard telephone network landline service.

Illustration for Packet Switching:



![alt_text](./img/image8.png "image_tooltip")


Packet switching networks are efficient for data that can tolerate transmission delays, such as e-mail message, site data, and become the basis for modern Wide Area Network (WAN) protocols.

**ISP**

End systems—devices that connected to the network such as laptops, mobile phones, TVs, and other IoT (Internet of Things)—access the internet through Internet Service Providers (ISPs) (Kurose & Ross, 2017, p. 32). ISP refers to companies or organizations that provide people with the internet. In Indonesia, companies that provide ISP are Telkom, Biznet, FirstMedia, and so on.

**National Internet Service Providers:**

The national Internet service providers are backbone networks created and maintained by specialized companies. There are many national ISPs operating in North America; some of the most well known are SprintLink, PSINet, UUNet Technology, AGIS, and internet Mel. To 

provide connectivity between the end users, these backbone networks are connected by complex switching stations (normally run by a third party) called network access points (NAPs). Some 

national ISP networks are also connected to one another by private switching stations called peering points. These normally operate at a high data rate (up to 600 Mbps).

**International Internet Service Providers:**

At the top of the hierarchy are the international service providers that connect nations together.

**Regional Internet Service Providers:** Regional internet service providers or regional ISPs are smaller ISPs that are connected to one or more national ISPs. They are at the third level of the hierarchy with a smaller data rate. 

**Local Internet Service Providers: **

Local Internet service providers provide direct service to the end users. The local ISPs can be connected to regional ISPs or directly to national ISPs. Most end users are connected to the local ISPs. Note that in this sense, a local ISP can be a company that just provides Internet services, a corporation with a network that supplies services to its own employees, or a nonprofit organization, such as a college or a university, that runs its own network. Each of these local ISPs can be connected to a regional or national service provider. 

Delays in Computer Network:

The delays, here, means the time for which the processing of a particular packet takes place. We have the following types of delays in computer networks:

1. Transmission Delay: 

The time taken to transmit a packet from the host to the transmission medium is called Transmission delay.



![alt_text](./img/image9.png "image_tooltip")


EXAMPLE:

If bandwidth is 1 bps (every second 1 bit can be transmitted onto the transmission medium) and data size is 20 bits then what is the transmission delay? If in one second, 1 bit can be transmitted. To transmit 20 bits, 20 seconds would be required.

2. Propagation Delay

After the packet is transmitted to the transmission medium, it has to go through the medium to reach the destination. Hence the time taken by the last bit of the packet to reach the destination is called propagation delay.



![alt_text](./img/image10.png "image_tooltip")


Factors affecting propagation delay:  

Distance – It takes more time to reach the destination if the distance of the medium is longer. 

Velocity – If the velocity(speed) of the signal is higher, the packet will be received faster. 

Tp = Distance / Velocity

3. Queueing delay: 

Let the packet is received by the destination, the packet will not be processed by the destination immediately. It has to wait in a queue in something called a buffer. So the amount of time it waits in queue before being processed is called queueing delay.

4. Processing delay: 

Now the packet will be taken for the processing which is called processing delay.

Time is taken to process the data packet by the processor that is the time required by intermediate routers to decide where to forward the packet, update TTL, perform header checksum calculations.

 It also doesn’t have any formula since it depends upon the speed of the processor and the speed of the processor varies from computer to computer. 

**Packet Loss**

Packet loss refers to the number of data packets that get lost or are dropped during their travel across a computer network before reaching their destination.

Latency – internet packet loss, so sometimes packet get lost in transit during their voyage.

Unsuccessful packets slow down network speeds, cause bottlenecks, and throw off your network throughput and bandwidth. Packet loss can also be expensive.

To reduce packet loss:



1. Check connections
2. Restart your system
3. Try cable connections instead of WiFi
4. Remove anything capable of causing static
5. Update software
6. Replace out of date hardware
7. Use QoS Settings

**Throughput:**

It can be defined as an amount of material transferred through a system or a process itself. Here we could call it as a “packet” for the material. The system through which the packet is passed can be a "link" which is Physical or Virtual.

Key Explanation: It is the measurement of how fast data can pass through an entity. 