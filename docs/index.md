<!-- # Welcome to MkDocs

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files. -->
**Lecture Notes**

**Communication and Data Networks**

**Built by:**

**Evan Christopher Samosir - 2006597065**

**Zachra Shafira Ramadhanty - 2006597033**

**Faculty of Computer Science**

**Universitas Indonesia**

**Ari Wibisono, S.Kom., M.Kom**

**March 3, 202**