**Module 4 - The Network Layer**

In a network with two hosts and routers in between, the network layer in Host 1 encapsulates segments into datagrams and sends them to the nearby router, while the network layer in Host 2 receives the datagrams from its nearby router, extracts the segments, and delivers them to the transport layer. The role of the routers is to forward datagrams from input links to output links. Routers do not run application- and transport-layer protocols.

See picture below to see the example of network layer.


![alt_text](./img/image20.png "image_tooltip")


**Forwarding and Routing**

The network layer has two main functions: forwarding and routing. Forwarding moves packets locally from input to output links, while routing determines the end-to-end paths packets take from source to destination. Each router has a forwarding table, which it uses to forward packets based on a field in the packet header. The routing algorithm determines the values in the forwarding tables using centralized or decentralized methods and configures them using routing protocol messages. While the terms forwarding, routing, packet switch, and router are often used interchangeably, they have distinct meanings. Forwarding refers to local packet transfer, routing refers to network-wide path determination, packet switch is a general device for packet transfer, and a router is a specific type of packet switch that operates at the network layer.

**Virtual Circuit and Datagram Network**

The network layer offers connectionless and connection-oriented services like the transport layer. The services are provided host-to-host by the network layer and process-to-process by the transport layer. The network layer provides either a host-to-host connectionless or connection service, not both. Virtual-circuit networks provide a connection service while datagram networks provide a connectionless service. These two types of networks use different information to make their forwarding decisions. The implementations of connection-oriented service in the transport layer and connection service in the network layer are fundamentally different. The network-layer connection service is implemented in routers and end systems while the transport-layer connection-oriented service is only implemented in end systems.

A virtual circuit (VC) is a type of network connection that is established between two endpoints in a virtual-circuit network. It is a logical connection that provides a connection-oriented service at the network layer. Whereas, in a datagram network, each packet is treated independently and is routed based on the packet's destination address. The network does not set up a dedicated path for the packets before they are sent, and each packet can potentially take a different path to reach its destination. In a datagram network, routers do not maintain any state information about the packets that they are forwarding.

**The Internet Protocol (IP): Forwarding and Addressing in the Internet**

See picture below of a look inside the internet’s network layer: \

![alt_text](./img/image21.png "image_tooltip")


The Internet's network layer has three components: the IP protocol, the routing component, and a facility for error reporting and information responding called ICMP.

A network-layer packet is referred to as a datagram. See picture below to see the format of a datagram:


![alt_text](./img/image22.png "image_tooltip")


Note that an IP datagram has a total of 20 bytes of header (assuming no options). If the datagram carries a TCP segment, then each (nonfragmented) datagram carries a total of 40 bytes of header (20 bytes of IP header plus 20 bytes of TCP header) along with the application-layer message.

**IP Datagram Fragmentation**

In IP networking, the maximum data that a link-layer frame can carry is called the maximum transmission unit (MTU). If an IP datagram is larger than the MTU of the link-layer protocol, it needs to be fragmented into smaller IP datagrams before being sent over the outgoing link. These smaller datagrams are called fragments. To identify whether a datagram is a fragment, the IP datagram header contains identification, flag, and fragmentation offset fields. The sending host stamps each datagram with an identification number, and the router stamps each fragment with the identification number of the original datagram. The offset field specifies where the fragment fits within the original IP datagram. At the destination, the IP layer fully reconstructs the original datagram before passing it to the transport layer. If any fragments are lost, the incomplete datagram is discarded, and if TCP is being used, the source retransmits the data.

**IPv4 Addressing**

An interface is the boundary between a host or a router and the physical link. A router necessarily has two or more links to which it is connected. Each host and router interface must have its own IP address. An IP address is 32 bits long (4 bytes), and there are a total of 4 billion possible IP addresses. These addresses are written in dotted-decimal notation, where each byte of the address is written in its decimal form and separated by a period from other bytes in the address. An interface's IP address is globally unique and cannot be chosen randomly.

IP addressing assigns an address to a subnet. A subnet is a group of interconnected host and router interfaces. A subnet is also called an IP network or simply a network in the Internet literature. The subnet address is determined by the leftmost bits of the 32-bit quantity. For example, the subnet 223.1.1.0/24 consists of the three host interfaces (223.1.1.1, 223.1.1.2, and 223.1.1.3) and one router interface (223.1.1.4). Any additional hosts attached to the 223.1.1.0/24 subnet would be required to have an address of the form 223.1.1.xxx.

**Routing Algorithm \
**Forwarding involves indexing a forwarding table to determine the link interface to which a packet should be directed. Routing algorithms are used to compute the information that is used to configure these forwarding tables. The network layer must determine the path that packets take from senders to receivers, and the purpose of a routing algorithm is to find a good path from the source router to the destination router.

A graph is used to formulate routing problems, with nodes representing routers and edges representing physical links between routers. Real-world concerns such as policy issues can complicate the algorithms used for routing in today's networks.