**Module 2 - Application Layer**

**Principles of Network Applications**

The process of network application development consists of writing programs that run on different end systems that communicate with each other over the network. When developing a new application, the developers need to write software that is able to run on multiple end systems. Developers do not need to write software that runs on network core devices like routers or link-layer switches. Network-core devices function at lower layer and below. Communication for a network application takes place between end systems at the application layer (see picture below).

![alt_text](./img/image11.png "image_tooltip")


**Network Application Architecture**

The application architecture tells how the application is structured over the various end system. There are two predominant architectural paradigm: the client-server architecture or the peer-to-peer architecture.

**Process Communicating**

In a network application, pairs of processes communicate over a network, where one process is labeled as the client and the other as the server. The client initiates contact with the server to begin a communication session. In P2P file sharing, a process can be both a client and a server, but in a specific communication session, one process is still labeled as the client and the other as the server. The terminology "client side and server side" is also used to refer to the two processes in an application.

Sockets act as a software interface for sending and receiving messages between pairs of processes in a network.  A socket is an interface between the application layer and the transport layer within a host which is identified by its IP address.

**Transport Services Available to Applications**

 \
The possible services offered by transport-layer protocol to applications can be classified into four dimensions: reliable data transfer, throughput, timing, and security.

**Throughput**

Throughput is the rate at which bits are delivered from sender to receiver. This rate can vary due to other sessions sharing the bandwidth. To help bandwidth-sensitive applications, a transport protocol can provide a guaranteed throughput service. Elastic applications can use whatever throughput is available. Examples include email, file transfer, and web transfers. The more throughput available, the better.

**Timing**

Transport-layer protocols can provide timing guarantees for interactive real-time applications, such as internet telephony, virtual environments, teleconferencing, and multiplayer games. 

**Security**

A transport protocol can offer security services to applications. These services may include encrypting data transmitted by the sending process and decrypting the data before delivering it to the receiving process, ensuring confidentiality between the two processes. Other security services such as data integrity and end-point authentication can also be provided by the transport protocol.

**Transport Services Provided by the Internet**

**TCP**

TCP provides a connection-oriented service and a reliable data transfer service. The connection-oriented service involves a handshaking procedure between the client and server to prepare for the transfer of messages. After the handshaking phase, the two processes can send messages to each other over the connection at the same time.

**UDP**

UDP is a simple and lightweight transport protocol that provides minimal services. It is connectionless, so there is no handshaking required, and it provides an unreliable data transfer service, meaning there are no guarantees that the message will ever reach the receiving process. There is also no congestion control mechanism in UDP, which means that the sending side of UDP can transmit data at any rate it chooses. 

**Application Layer Protocols**

An application-layer protocol defines how an application’s processes, running on different end systems, pass messages to each other.

**The Web and HTTP**

HTTP is the application-layer protocol used by the Web. It is implemented by a client program and a server program that exchange HTTP messages. A Web page consists of objects, such as HTML files, images, and video clips, that are addressable by a single URL. The URL has two components: the hostname of the server that houses the object and the object's path name.

**HTTP Response Message**

A typical HTTP response message example: 

HTTP/1.1 200 OK

Connection: close

![alt_text](./img/image12.png "image_tooltip")


The picture above shows the general format of an HTTP response message.

Some of the most common status codes and associated phrases are:



*  200 OK: Request succeeded and the information is returned in the response.
* 301 Moved Permanently: Requested object has been permanently moved; the new URL is specified in Location: header of the response message. The client software will automatically retrieve the new URL.
* 400 Bad Request: This is a generic error code indicating that the request could not be understood by the server.
* 404 Not Found: The requested document does not exist on this server.
* 505 HTTP Version Not Supported: The requested HTTP protocol version is not supported by the server.

**Domain Name System (DNS)**

DNS is a system that translates hostnames to IP addresses by using a distributed database implemented in a hierarchy of DNS servers and an application-layer protocol that allows hosts to query the distributed database. DNS is commonly employed by other application-layer protocols to translate user-supplied hostnames to IP addresses. DNS adds an additional delay to internet applications that use it, but the desired IP address is often cached in a nearby DNS server to reduce DNS network traffic as well as the average DNS delay. DNS also provides host aliasing services that allow a complicated hostname to have one or more alias names.

**Peer-to-Peer Applications**

This section discusses peer-to-peer (P2P) architectures, where intermittently connected hosts communicate directly with each other, without significant reliance on always-on infrastructure servers. Two applications well-suited for P2P designs are file distribution and databases distributed over large communities of peers. The section compares client-server and P2P architectures using a simple quantitative model for distributing a file to a fixed set of peers.

**Socket Programming**

**Socket Programming with UDP**

When two processes communicate using UDP sockets, the sending process must first attach a destination address to the packet. The destination address includes the destination host's IP address and the destination socket's port number. Additionally, the source address, consisting of the IP address of the source host and the port number of the source socket, is automatically attached by the operating system. A client-server application is used to demonstrate socket programming for both UDP and TCP, and the program pair for a UDP implementation is provided. The UDP client sends a simple message to the server, which must be running before the message is sent.

**Socket Programming with TCP**

TCP is a connection-oriented protocol where the client and server must first establish a TCP connection before sending data. The client initiates contact with the server by creating a TCP socket and specifying the address of the welcoming socket in the server. During the three-way handshake, the server creates a new socket dedicated to the client, and from the application's perspective, the client's socket and the server's connection socket are directly connected by a pipe. TCP provides a reliable service between the client and server processes. In a simple client-server application, the client sends one line of data to the server, the server capitalizes the line, and sends it back to the client.